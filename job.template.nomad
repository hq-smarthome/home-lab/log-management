job "log-management" {
  type = "service"
  region = "[[ .defaultRegion ]]"
  datacenters = ["[[ .defaultDatacenter ]]"]
  priority = "[[ .defaultPriority ]]"

  group "loki" {
    count = 1

    volume "loki" {
      type = "host"
      read_only = false
      source = "loki"
    }

    network {
      mode = "bridge"
    }

    service {
      name = "loki"
      port = "3100"
      task = "loki"

      connect {
        sidecar_service {}
      }

      check {
        expose = true
        name = "Application Ready Status"
        type = "http"
        path = "/ready"
        interval = "10s"
        timeout = "3s"
      }

      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=true",
        "traefik.http.routers.loki.entrypoints=https",
        "traefik.http.routers.loki.rule=Host(`loki.hq.carboncollins.se`)",
        "traefik.http.routers.loki.tls=true",
        "traefik.http.routers.loki.tls.certresolver=lets-encrypt",
        "traefik.http.routers.loki.tls.domains[0].main=*.hq.carboncollins.se"
      ]
    }

    task "loki" {
      driver = "docker"
      user = "[[ .defaultUserId ]]"

      config {
        image = "grafana/loki:2.3.0"

        volumes = [
          "local/loki/local-config.yaml:/etc/loki/local-config.yaml"
        ]
      }

      volume_mount {
        volume = "loki"
        destination = "/loki"
        read_only = false
      }

      template {
        data = <<EOH
[[ fileContents "./config/loki.template.yaml" ]]
        EOH

        destination = "local/loki/local-config.yaml"
      }
    }
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
