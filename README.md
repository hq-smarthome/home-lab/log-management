# Log Management

> :warning: **This project has been moved**
>
> Any further updates can be found here https://gitlab.com/carboncollins-cloud/log-management
> This repository will be archived in favour of all further development at the new location.
